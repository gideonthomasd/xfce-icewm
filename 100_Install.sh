#!/bin/bash

sudo apt update
sudo apt install jq audacious playerctl lua5.4 webp-pixbuf-loader -y
sudo apt install rofi geany pcmanfm -y
sudo apt install conky exa -y
sudo apt install libasan6 -y

### Set up geany and neofetch ### 
mkdir -p ~/.config/geany/colorschemes
cd colorschemes
cp -r * ~/.config/geany/colorschemes
cd ..
cp geany.conf ~/.config/geany/geany.conf

mkdir -p ~/.config/neofetch
cp config.conf ~/.config/neofetch/config.conf

### Set up my rofi ###
mkdir -p ~/.config/rofi/myrofi
cd myrofi
cp -r * ~/.config/rofi/myrofi
cd ..

### Set up some fonts ###
mkdir -p ~/.local/share/fonts
cd fonts
cp -r * ~/.local/share/fonts
cd ..

### Set up my conky ###
mkdir -p ~/.config/conky
cd conky
cp -r * ~/.config/conky
cd ..

### Starship ###
cp starship.toml ~/.config/starship.toml

### Wallpaper ###
mkdir -p ~/wallpaper
cd wallpaper
cp -r * ~/wallpaper
cd ..

### Themes ###
mkdir -p ~/.icons/Bibata-Modern-Ice
mkdir -p ~/.themes
cp -r ZorinBlue-Dark  ~/.themes/ZorinBlue-Dark
cp -r ZorinGreen-Dark  ~/.themes/ZorinGreen-Dark
cp -r ZorinGrey-Dark  ~/.themes/ZorinGrey-Dark
cp -r ZorinOrange-Dark  ~/.themes/ZorinOrange-Dark
cp -r ZorinRed-Dark  ~/.themes/ZorinRed-Dark
cp -r ZorinPurple-Dark  ~/.themes/ZorinPurple-Dark

### Icons ###
cd icons
cp -r ZorinPurple-Dark ~/.icons/ZorinPurple-Dark
cd ..

tar -xzvf kora.tar.gz
tar -xzvf candy-icons.tar.gz
tar -xzvf Dracula.tar.gz
tar -xzvf Sweet-Dark.tar.gz

cp -r kora ~/.icons/kora
cp -r candy-icons ~/.icons/candy-icons
cp -r Dracula ~/.themes/Dracula
cp -r Sweet-Dark ~/.themes/Sweet-Dark

### Music ###
cp Praise.mp3 ~/Music/Praise.mp3

### Cursor ###
cd Bibata-Modern-Ice
cp -r * ~/.icons/Bibata-Modern-Ice
cd ..


cp bashrc ~/.bashrc
cp Xresources ~/.Xresources

mkdir -p ~/.local/share/xfce4/terminal/colorschemes
cp Dracula.theme ~/.local/share/xfce4/terminal/colorschemes/Dracula.theme
