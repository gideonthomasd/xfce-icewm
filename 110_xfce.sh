#!/bin/bash

sudo apt install xfce4-whiskermenu-plugin xfce4-weather-plugin xfce4-genmon-plugin volumeicon-alsa exa -y
#cp menu-bar.png ~/Pictures/menu-bar.png
sudo apt install xfce4-terminal -y
cp music.png ~/Pictures/music.png

mv ~/.config/xfce4 ~/.config/xfce4-old
mkdir -p ~/.config/xfce4
cd xfce4
cp -r * ~/.config/xfce4
cd ..


echo "In startup alter menu.  Add >>"
echo "volumeicon [name=volumeicon-alsa]"
echo "sh /home/phil/.config/conky/Mimosa/start.sh"
echo "audacious [name = audacious].  Change so stays in taskbar"
echo "Do ip link in terminal and change mimosa.conf"
