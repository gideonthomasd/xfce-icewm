#!/bin/bash

sudo apt install icewm tint2 i3lock-fancy nitrogen htop parcellite gsimplecal lxappearance lxtask -y

sudo cp zentile /usr/bin/zentile

mkdir -p ~/.icewm
cd icewm
cp -r * ~/.icewm
cd ..

cd thisrofi
cp -r * ~/.config/rofi
cd ..

mkdir -p ~/.config/tint2
cd tint2
cp -r * ~/.config/tint2
cd ..

### Fonts ###
mkdir -p ~/.local/share/fonts
cd Mononoki
cp -r * ~/.local/share/fonts
cd ..
