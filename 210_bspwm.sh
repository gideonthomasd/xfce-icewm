#!/bin/bash

sudo apt install bspwm sxhkd polybar cava -y

mkdir -p ~/.config/bspwm
mkdir -p ~/.config/polybar
mkdir -p ~/bin
mkdir -p ~/.config/eww
mkdir -p ~/.config/picom

cp picom.conf ~/.config/picom/picom.conf

cd bspwm
cp -r * ~/.config/bspwm
cd ..

cd polybar
cp -r * ~/.config/polybar
cd ..

cd firacode
cp -r * ~/.local/share/fonts
cd ..

cd bin
cp -r * ~/bin
cd ..

cd eww
cp -r * ~/.config/eww
cd ..
