#!/bin/bash

sudo apt install openbox obconf htop gsimplecal parcellite tint2 dmenu -y

### Tint2 ###
mkdir -p ~/.config/tint2
cd tint2
cp -r * ~/.config/tint2
cd ..

### Openbox ###
mkdir -p ~/.config/openbox
cd openbox
cp -r * ~/.config/openbox
cd ..

### Fonts ###
mkdir -p ~/.local/share/fonts
cd Mononoki
cp -r * ~/.local/share/fonts
cd ..

### Themes ###
mkdir -p ~/.themes/Prismatic-Night
cd Prismatic-Night
cp -r * ~/.themes/Prismatic-Night
cd ..

mkdir -p ~/.themes/Adapta-Nokto
cd Adapta-Nokto
cp -r * ~/.themes/Adapta-Nokto
cd ..

