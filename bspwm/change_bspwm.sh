#!/bin/bash

one="1 6 bars"
two="2 Circles on LHS."
three="3 Squares in middle"
four="4 Tint2"
five="5 Volume in center"
six="6 Pamela"
seven="7 Karla"
# Get answer from user via rofi
selected_option=$(echo "$one
$two
$three
$four
$five
$six
$seven" | rofi -dmenu\
                  -i\
                  -p "Bspwm Desktop"\
                  -config "~/.config/rofi/powermenu.rasi"\
                  -font "JetBrainsMono Nerd Font 18"\
                  -width "15"\
                  -lines 7\
                  -line-margin 3\
                  -line-padding 10\
                  -scrollbar-width "0" )

# Do something based on selected option
if [ "$selected_option" == "$one" ]
then
    sh ~/.config/bspwm/rice/1/polybar/launch.sh
    echo "#!/bin/bash" > ~/.config/bspwm/mystart.sh
    echo "~/.config/bspwm/rice/1/polybar/launch.sh" >> ~/.config/bspwm/mystart.sh
elif [ "$selected_option" == "$two" ]
then
    sh ~/.config/bspwm/rice/2/polybar/launch.sh
    echo "#!/bin/bash" > ~/.config/bspwm/mystart.sh
    echo "~/.config/bspwm/rice/2/polybar/launch.sh" >> ~/.config/bspwm/mystart.sh
elif [ "$selected_option" == "$three" ]
then
    sh ~/.config/bspwm/rice/3/polybar/launch.sh
    echo "#!/bin/bash" > ~/.config/bspwm/mystart.sh
    echo "~/.config/bspwm/rice/3/polybar/launch.sh" >> ~/.config/bspwm/mystart.sh
elif [ "$selected_option" == "$four" ]
then
    sh ~/.config/bspwm/rice/4/polybar/launch.sh
    echo "#!/bin/bash" > ~/.config/bspwm/mystart.sh
    echo "~/.config/bspwm/rice/4/polybar/launch.sh" >> ~/.config/bspwm/mystart.sh
elif [ "$selected_option" == "$five" ]
then
    sh ~/.config/bspwm/rice/5/polybar/launch.sh
    echo "#!/bin/bash" > ~/.config/bspwm/mystart.sh
    echo "~/.config/bspwm/rice/5/polybar/launch.sh" >> ~/.config/bspwm/mystart.sh
elif [ "$selected_option" == "$six" ]
then
    sh ~/.config/bspwm/rice/6/polybar/launch.sh
    echo "#!/bin/bash" > ~/.config/bspwm/mystart.sh
    echo "~/.config/bspwm/rice/6/polybar/launch.sh" >> ~/.config/bspwm/mystart.sh
elif [ "$selected_option" == "$seven" ]
then
    sh ~/.config/bspwm/rice/7/polybar/launch.sh
    echo "#!/bin/bash" > ~/.config/bspwm/mystart.sh
    echo "~/.config/bspwm/rice/7/polybar/launch.sh" >> ~/.config/bspwm/mystart.sh
else
    echo "No match"
fi

pkill change_bspwm.sh

