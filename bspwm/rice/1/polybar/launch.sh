#!/bin/bash

killall -q polybar
killall tint2
# If all your bars have ipc enabled, you can also use
# polybar-msg cmd quit

# Launch Polybar, using default config location ~/.config/polybar/config.ini
#polybar cata-bar 2>&1 | tee -a /tmp/polybar.log & disown
#polybar cata-bar -c ~/.config/polybar/config.ini 
polybar pam1 -c ~/.config/bspwm/rice/1/polybar/config &
polybar pam2 -c ~/.config/bspwm/rice/1/polybar/config &
polybar pam3 -c ~/.config/bspwm/rice/1/polybar/config &
polybar pam4 -c ~/.config/bspwm/rice/1/polybar/config &
polybar pam5 -c ~/.config/bspwm/rice/1/polybar/config &
polybar pam6 -c ~/.config/bspwm/rice/1/polybar/config &


echo "Polybar launched..."
