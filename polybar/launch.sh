#!/bin/bash

killall -q polybar
# If all your bars have ipc enabled, you can also use
# polybar-msg cmd quit

# Launch Polybar, using default config location ~/.config/polybar/config.ini
#polybar cata-bar 2>&1 | tee -a /tmp/polybar.log & disown
polybar cata-bar -c ~/.config/polybar/config.ini 

echo "Polybar launched..."
