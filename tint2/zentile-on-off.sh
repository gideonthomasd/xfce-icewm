#!/bin/bash

SERVICE="zentile"
if pgrep -x "$SERVICE" >/dev/null
then
	pkill zentile
else
	zentile
fi
