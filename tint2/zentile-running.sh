#!/bin/bash

SERVICE="zentile"
if pgrep -x "$SERVICE" >/dev/null
then
	echo "ON"
else
	echo "OFF"
fi
